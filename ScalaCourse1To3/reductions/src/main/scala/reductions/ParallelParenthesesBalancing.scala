package reductions

import common._
import org.scalameter._

object ParallelParenthesesBalancingRunner {

  @volatile var seqResult = false

  @volatile var parResult = false

  val standardConfig = config(
    Key.exec.minWarmupRuns -> 40,
    Key.exec.maxWarmupRuns -> 80,
    Key.exec.benchRuns -> 120,
    Key.verbose -> true
  ) withWarmer (new Warmer.Default)

  def main(args: Array[String]): Unit = {
    val length = 100000000
    val chars = new Array[Char](length)
    val threshold = 10000
    val seqtime = standardConfig measure {
      seqResult = ParallelParenthesesBalancing.balance(chars)
    }
    println(s"sequential result = $seqResult")
    println(s"sequential balancing time: $seqtime ms")

    val fjtime = standardConfig measure {
      parResult = ParallelParenthesesBalancing.parBalance(chars, threshold)
    }
    println(s"parallel result = $parResult")
    println(s"parallel balancing time: $fjtime ms")
    println(s"speedup: ${seqtime / fjtime}")
  }
}

object ParallelParenthesesBalancing {

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
    */
  def balance(chars: Array[Char]): Boolean = {
    val (count, ordering) = chars.foldLeft((0, true)) { case ((acc, o), c) =>
      c match {
        case '(' => (acc + 1, o)
        case ')' if acc == 0 => (acc - 1, false)
        case ')' => (acc - 1, o)
        case _ => (acc, o)
      }
    }
    count == 0 && ordering
  }

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
    */
  def parBalance(chars: Array[Char], threshold: Int): Boolean = {

    case class CountMin(
      count: Int,
      min: Int
    ) {
      val isBalanced: Boolean = count == 0 && min >= 0
    }

    def traverse(idx: Int, until: Int, count: Int, min: Int): CountMin = {
      if (until - idx <= threshold) {
        reduce(idx, until)
      } else {
        val mid = (until + idx) / 2
        val (left, right) = parallel(
          traverse(idx, mid, count, min),
          traverse(mid, until, count, min)
        )
        //On combining left and right, the 'global' min is the lowest overall on combining back left and right
        // so is the minimum of left ( = left.min) and right ( = count on the left + min on the right)
        //
        // e.g. ') ((())) ) ((((' => say, left = ') ((', right - '())) ) (((('
        //   left: count = 1, min = -1
        //   right: count = 2, min = -3
        // => combination: count = 3, min = -1 vs (1 + -3) = -2
        CountMin(
          count = left.count + right.count,
          min = Math.min(left.min, left.count + right.min)
        )
      }
    }

    def reduce(from: Int, until: Int): CountMin = {
      chars.foldLeft(CountMin(0, 0)) { case (CountMin(accCount, globalMin), c) =>
        c match {
          case '(' => CountMin(accCount + 1, globalMin)
          case ')' => CountMin(accCount - 1, Math.min(globalMin, accCount - 1)) //Only decrease 'global' min if we are already sitting at the min
          case _ => CountMin(accCount, globalMin)
        }
      }
    }

    val output = traverse(0, chars.length, 0, 0)

    //Last step is to convert the CountMin pair to a isBalanced boolean
    output.isBalanced
  }

  // For those who want more:
  // Prove that your reduction operator is associative!

}
