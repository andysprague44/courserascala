package reductions

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class LineOfSightSuite extends FunSuite {

  import reductions.LineOfSight._

  test("lineOfSight should correctly handle an array of size 4") {
    val output = new Array[Float](4)
    lineOfSight(Array[Float](0f, 1f, 8f, 9f), output)
    assert(output.toList == List(0f, 1f, 4f, 4f))
  }


  test("upsweepSequential should correctly handle the chunk 1 until 4 of an array of 4 elements") {
    val res = upsweepSequential(Array[Float](0f, 1f, 8f, 9f), 1, 4)
    assert(res == 4f)
  }

  test("upsweepSequential should correctly handle array 1 until 5 of an array of 5 elements for threshold 1") {
    val res = upsweepSequential(Array[Float](0f, 1f, 8f, 9f, 10f), 1, 5)
    assert(res == 4f)
  }

  test("downsweepSequential should correctly handle a 4 element array when the starting angle is zero") {
    val output = new Array[Float](4)
    downsweepSequential(Array[Float](0f, 1f, 8f, 9f), output, 0f, 1, 4)
    assert(output.toList == List(0f, 1f, 4f, 4f))
  }

  test("parLineOfSight in parts") {
    val arr = Array[Float](0f, 1f, 8f, 9f)
    val out = new Array[Float](4)
    val tree = upsweep(arr, 0, 4, 2)
    downsweep(arr, out, 0, tree)
    assert(out.max == 4f)
  }

  test("parLineOfSight threshold 3") {
    val arr = Array[Float](0f, 9f, 16f, 15f, 32f, 40f)
    val out = new Array[Float](6)
    parLineOfSight(arr, out, 3)
    assert(out.toList == List(0f, 9f, 9f, 9f, 9f, 9f))
  }

  test("upsweep should correctly handle array 1 until 5 of an array of 5 elements for threshold 1") {
    val tree = upsweep(Array[Float](0f, 1f, 8f, 9f, 12f), 0, 5, 1)
    assert(tree == Node(Node(Leaf(0, 1, 0f), Leaf(1, 2, 1f)), Node(Leaf(2, 3, 4f), Node(Leaf(3, 4, 3f), Leaf(4, 5, 3f)))))
  }

}

