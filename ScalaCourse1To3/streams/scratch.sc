

//http://codegolf.stackexchange.com/questions/100486/make-a-ceeeeeeee-program/100799#100799
def dNonGolf(s: String, c: Char) = {
  (1 until s.length).foldLeft((s.take(1), List[String]())) { case ((prefix, acc), i) =>
    s.charAt(i) match {
      case char if char != c => (prefix, acc :+ (prefix + s.drop(i)))
      case char => (prefix + char, acc)
    }
  }._2
}

def d(s: String, c: Char) = {
  1.to(s.size-1).foldLeft(s.take(1)){(p,i)=>val x=s.charAt(i);if(x!=c){println(p+s.drop(i));p}else p+x}
}

d("Test Cases", 's')

