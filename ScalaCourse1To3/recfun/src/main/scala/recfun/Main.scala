package recfun

import scala.annotation.tailrec

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
    * Exercise 1
    */
  def pascal(c: Int, r: Int): Int = {

    @tailrec
    def pascalIter(acc: Array[Int] = Array(1), row: Int = 1): Array[Int] = {
      if (row > r) acc
      else {
        val nextRow = 1 +: acc.iterator.sliding(2).withPartial(false).map(_.sum).toArray :+ 1
        pascalIter(nextRow, row + 1)
      }
    }

    pascalIter()(c)

  }

  /**
    * Exercise 2
    */
  def balance(chars: List[Char]): Boolean = {

    val balanced = chars.foldLeft(0) {
      case (-1, _) => -1 //have closed a 'not open' bracket, which is always invalid
      case (count, ')') => count - 1
      case (count, '(') => count + 1
      case (count, _) => count
    }

    balanced == 0

  }

  /**
    * Exercise 3
    */
  def countChange(money: Int, coins: List[Int]): Int = {

    /** Repeat each denomination to upper bound of 'money' */
    def fillCoins(i: Int): List[Int] = List.fill(Math.floorDiv(money, i))(i)

    /** Largest valid size of possible sub-set */
    def upperBound(coins: List[Int]): Int = Math.min(coins.length, Math.floorDiv(money, coins.min))

    /** Count valid subsets of a particular length */
    def countValidCombinations(coins: List[Int])(len: Int): Int = {
      coins.combinations(len).count(_.sum == money)
    }

    money match {
      case 0 => 1
      case m if coins.isEmpty => 0
      case m =>
        val possibleCoins = coins.flatMap(fillCoins)
        val range = 1 to upperBound(possibleCoins)
        range.map(countValidCombinations(possibleCoins)).sum
    }
  }


}
