package quickcheck

import org.scalacheck.Arbitrary._
import org.scalacheck.Gen._
import org.scalacheck.Prop._
import org.scalacheck._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = for {
    k <- arbitrary[Int]
    h <- oneOf(const(empty), genHeap)
  } yield insert(k, h)

  type P = (H, List[Int])
  lazy val genHeapWithInputs: Gen[P] = for {
    k <- arbitrary[Int]
    (h, inputs) <- oneOf(const((empty, List())), genHeapWithInputs)
  } yield (insert(k, h), (k :: inputs).sorted(ord))

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  implicit lazy val arbHeapWithInputs: Arbitrary[P] = Arbitrary(genHeapWithInputs)

  property("gen1") = forAll { (h: H) =>
    val a = if (isEmpty(h)) 0 else findMin(h)
    val min = findMin(insert(a, h))
    min == a
  }

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    val min = findMin(h)
    min == a
  }

  property("min of 2 elements") = forAll { (a: Int, b: Int) =>
    val h = empty.add(a).add(b)
    val m = Math.min(a, b)
    val min = findMin(h)
    min == m
  }

  property("min of 2 elements genHeap") = forAll { (h: H) =>
    val a = if (isEmpty(h)) 0 else findMin(h)
    val b = Math.min(a, a - 1)
    val min = h.add(a).add(b).min()
    min == b
  }

  property("insert and delete") = forAll { (a: Int) =>
    val h = empty.add(a).dropMin()
    isEmpty(h)
  }

  property("insert and delete genHeap") = forAll { (h: H) =>
    val a = if (isEmpty(h)) 0 else findMin(h)
    val after = h.add(a).dropMin()
    after == h
  }

  property("minimum after melding") = forAll { (a: Int, b: Int) =>
    val m = Math.min(a, b)
    val min = findMin(meld(insert(a, empty), insert(b, empty)))
    min == m
  }

  property("minimum after melding genHeap") = forAll { (h1: H, h2: H) =>
    val m = Math.min(findMin(h1), findMin(h2))
    val min = findMin(meld(h1, h2))
    min == m
  }

  property("taking min until empty") = forAll { (pair: P) =>
    val (h, k) = pair
    val arr = takeAllInOrder(h)
    arr == k
  }
}
