import barneshut._
import barneshut.conctrees.ConcBuffer

val boundaries = new Boundaries()
boundaries.minY = 0
boundaries.minX = 0
boundaries.maxX= 12
boundaries.maxY = 12

val sectorPrecision = 4
val sectorSize = boundaries.size / sectorPrecision

val matrix = new Array[ConcBuffer[Body]](sectorPrecision * sectorPrecision)
for (i <- matrix.indices) matrix(i) = new ConcBuffer

val b = new Body(100, 2, 2, 0, 0)


val cornerMap = matrix.indices.map(i =>
  {
    val sectorCornerX = sectorSize * (i % sectorPrecision + 1)
    val sectorCornerY = sectorSize * ((i - i % sectorPrecision) / sectorPrecision + 1)
    val sectorMaxX = if (sectorCornerX == boundaries.maxX) Float.MaxValue else sectorCornerX
    val sectorMaxY = if (sectorCornerY == boundaries.maxY) Float.MaxValue else sectorCornerY
    i -> (sectorMaxX, sectorMaxY)
  }
)

 // if (b.x < boundaries.minX + sectorSize && b.y < boundaries.minY + sectorSize) matrix(0) += b
