package calculator

sealed abstract class Expr

final case class Literal(v: Double) extends Expr

final case class Ref(name: String) extends Expr

final case class Plus(a: Expr, b: Expr) extends Expr

final case class Minus(a: Expr, b: Expr) extends Expr

final case class Times(a: Expr, b: Expr) extends Expr

final case class Divide(a: Expr, b: Expr) extends Expr

object Calculator {
  def computeValues(
    namedExpressions: Map[String, Signal[Expr]]): Map[String, Signal[Double]] = {

    namedExpressions.map { case (v, s) =>
      val value = {
        if (hasCircularReferences(List(v), s(), namedExpressions))
          Signal(Double.NaN)
        else
          Signal(eval(s(), namedExpressions))
      }
      (v, value)
    }
  }

  def eval(expr: Expr, references: Map[String, Signal[Expr]]): Double = {
    expr match {
      case Literal(v) => v
      case Ref(name) => eval(getReferenceExpr(name, references), references)
      case Plus(a, b) => eval(a, references) + eval(b, references)
      case Minus(a, b) => eval(a, references) - eval(b, references)
      case Times(a, b) => eval(a, references) * eval(b, references)
      case Divide(a, b) => eval(a, references) / eval(b, references)
    }
  }

  def hasCircularReferences(v: List[String], expr: Expr, references: Map[String, Signal[Expr]]): Boolean = {
    expr match {
      case Ref(name) if v.contains(name) => true
      case Ref(name) => hasCircularReferences(v :+ name, getReferenceExpr(name, references), references)
      case Literal(_) => false
      case Plus(a, b) => hasCircularReferences(v, a, references) || hasCircularReferences(v, b, references)
      case Minus(a, b) => hasCircularReferences(v, a, references) || hasCircularReferences(v, b, references)
      case Times(a, b) => hasCircularReferences(v, a, references) || hasCircularReferences(v, b, references)
      case Divide(a, b) => hasCircularReferences(v, a, references) || hasCircularReferences(v, b, references)
    }
  }

  /** Get the Expr for a referenced variables.
    * If the variable is not known, returns a literal NaN.
    */
  private def getReferenceExpr(name: String,
    references: Map[String, Signal[Expr]]) = {
    references.get(name).fold[Expr] {
      Literal(Double.NaN)
    } { exprSignal =>
      exprSignal()
    }
  }
}
