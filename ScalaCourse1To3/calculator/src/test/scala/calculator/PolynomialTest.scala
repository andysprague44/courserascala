package calculator

import org.scalatest.{Matchers, FunSuite}

class PolynomialTest extends FunSuite with Matchers {

  test("testComputeSolutions") {
    val a = Var(1.0)
    val b = Var(1.0)
    val c = Var(1.0)
    val result = Polynomial.computeSolutions(a, b, c, Polynomial.computeDelta(a, b, c))
    result() shouldBe Set()
    a() = 2
    b() = 1
    c() = -3
    result() shouldBe Set(1.0, -1.5)
  }

}
