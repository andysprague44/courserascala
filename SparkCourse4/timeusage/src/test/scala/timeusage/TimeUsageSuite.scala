package timeusage

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfterAll, FunSuite}

@RunWith(classOf[JUnitRunner])
class TimeUsageSuite extends FunSuite with BeforeAndAfterAll {
  import timeusage.TimeUsage.spark.implicits._

  test("testTimeUsageByLifePeriod_ElderlyMenVsWomen") {
    val df = TimeUsage.createTimeUsageByLifePeriodDf()

    df.where($"age" === "elderly").show()

  }


}
